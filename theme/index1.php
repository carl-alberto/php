<?php
/**
 * The main template file.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BootstrapFast
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if ( get_header_image() ) { ?>
				<div class="headercontainer">
					<img src="<?php echo( esc_html( get_header_image() ) ); ?>" alt="<?php echo( esc_html( get_bloginfo( 'title' ) ) ); ?>" />
				</div>
			<?php } ?>

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;

		if ( 1 === $test ) {
			echo 'test';
		}


		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
